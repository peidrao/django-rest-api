from django.utils.safestring import mark_safe
from django.conf import settings
from django.db import models

# Create your models here.

def upload_status_image(instance, filename):
    return 'updates/{user}/{filename}'.format(user=instance.user, filename=filename)

class StatusQuerySet(models.QuerySet):
    pass


class StatusManager(models.Manager):
    def get_queryset(self):
        return StatusQuerySet(self.model, using=self._db)


class Status(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    content  = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to=upload_status_image, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = StatusManager()

    def __str__(self):
        return str(self.content)[:50]
    

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="{}" height="50">'.format(self.image.url))

    class Meta:
        verbose_name = 'Status post'
        verbose_name_plural = 'Status posts'