import requests 

BASE_URL = 'http://127.0.0.1:8000/'

ENDPOINT = 'api/updates/'

def get_list():
    r = requests.get(BASE_URL + ENDPOINT)
    data = r.json()
    return data

#print(get_list())

def create_update():
    new_data = {
        'user': 1,
        'content': 'Another new cool test'
    }
    r = requests.post(BASE_URL + ENDPOINT + '1', data=new_data)
    print(r.headers)
    print(r.status_code)
    if r.status_code == requests.codes.ok:
        # print(r.json())
        return r.json()
    return r.text


print(create_update())