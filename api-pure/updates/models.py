import json

from django.core.serializers import serialize
from django.conf import settings
from django.db import models


# Create your models here.

def upload_update_image(instance, filename):
    return 'updates/{user}/{filename}'.format(user=isinstance.user, filename=file)


class UpdateQuerySet(models.QuerySet):
    def serialize(self):
        list_values = list(self.values('id','user', 'content', 'image'))
        return json.dumps(list_values)

class UpdateManager(models.Manager):
    def get_queryset(self):
        return UpdateQuerySet(self.model, using=self._db)

class Update(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    content = models.TextField()
    image = models.ImageField(upload_to=upload_update_image, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UpdateManager()

    def __str__(self):
        return self.content or ""

    def serialize(self):
        try:
            image = self.image.url
        except:
            image = ""
        data = {
            'id': self.id,
            'user': self.user.username,
            'content': self.content,
            'image': image
        }
        data_json = json.dumps(data)
        return data_json