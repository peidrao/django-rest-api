import json

from django.views.generic import View
from django.http import HttpResponse

from ..forms import UpdateModelForm
from ..models import Update as UpdateModel
from ..mixins import CSRFExemptMixin
from ..mixins import HttpResponseMixin
from ..utils import is_json



class UpdateModelDetailAPIView(CSRFExemptMixin, HttpResponseMixin, View):
    """ Retrieve, Update, Delete -> Object  """
    is_json = True  

    def get_object(self, id=None):
        """ try:
            obj = UpdateModel.objects.get(id=id)
        except:
            obj = None """
        """ Below handles a does not exist Exception too """
        qs = UpdateModel.objects.filter(id=id)
        if qs.count() == 1:
            return qs.first()
        return None

    def get(self, request, id, *args, **kwargs):
        obj = UpdateModel.objects.get(id=id)
        if obj is None:
            error_data = json.dumps({'message': 'Update not found'})
            return self.render_to_response(error_data, status=404)
        json_data = obj.serialize()
        return self.render_to_response(json_data)
    
    def post(self, request, *args, **kwargs):
        json_data = json.dumps({'message': 'Not allowed, please use the create endpoint'})
        return self.render_to_response(json_data, status=403)

    def put(self, request, id, *args, **kwargs):
        obj = UpdateModel.objects.get(id=id)
        if obj is None:
            error_data = json.dumps({'message': 'Update not found'})
            return self.render_to_response(error_data, status=404)

        valid_json = is_json(request.body) 
        if not valid_json:
            error_data = json.dumps({'message': 'Invalid data sent, please send using JSON.'})
            return self.render_to_response(error_data, status=400)
        json_data = obj.serialize()
        return self.render_to_response(json_data)

    def delete(self, request, id, *args, **kwargs):
        obj = UpdateModel.objects.get(id=id)
        if obj is None:
            error_data = json.dumps({'message': 'Update not found'})
            return self.render_to_response(error_data, status=404)
        json_data = obj.serialize()
        return self.render_to_response(json_data)


class UpdateModelListAPIView(CSRFExemptMixin, HttpResponseMixin, View):
    """ List View & Create View """
    is_json = True

    def get(self, request, *args, **kwargs):
        qs = UpdateModel.objects.all()
        json_data = qs.serialize()
        return self.render_to_response(json_data)

    def post(self, request, *args, **kwargs):
        form = UpdateModelForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=True)
            obj_data = obj.serialize()
            return self.render_to_response(obj_data, status=201)
        if form.errors:
            data = json.dumps(form.errors)
            return self.render_to_response(data, status=400)
            
        data = {'message': 'Nor Allowed'}
        return self.render_to_response(data, status=400)
    
    def delete(self, request, *args, **kwargs):
        valid_json = is_json(request.body) 
        if not valid_json:
            error_data = json.dumps({'message': 'Invalid data sent, please send using JSON.'})
            return self.render_to_response(error_data, status=400)
        data = json.dumps({'message': 'You cannot delete an entire lists'})
        status_code = 403
        return self.render_to_response(data, status=403)