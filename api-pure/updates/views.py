import json

from django.core.serializers import serialize
from django.http import JsonResponse, HttpResponse
from django.template.loader import get_template
from django.shortcuts import render

from django.views.generic import View

# Create your views here.
from .models import Update
from .mixins import JsonResponseMixin

# 

def update_model_detail_view(request):
    data = {
        "count": 1000,
        "content": "Some new content"
    }
    return JsonResponse(data, safe=False)


class JsonView(View):
    def get(self, request, *args, **kwargs):
        data = {
        "count": 1000,
        "content": "Some new content"
        }
        return JsonResponse(data) 

class JsonView2(JsonResponseMixin, View):
    def get(self, request, *args, **kwargs):
        data = {
        "count": 1000,
        "content": "Some new content"
        }
        return self.render_to_json_response(data)


class SerializedDetailView(View):
    def get(self, request, *args, **kwargs):
        obj = Update.objects.get(id=1)
        json_data = obj.serialize()
        return HttpResponse(json_data, content_type='application/json')


class SerializerListView(View):
    def get(self, request, *args, **kwargs):
        json_data = Update.objects.all().serialize()
        return HttpResponse(json_data, content_type='application/json')
    