from django.contrib import admin
from django.urls import path, include

from updates.views import update_model_detail_view, JsonView, JsonView2, SerializedDetailView, SerializerListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/updates/', include('updates.api.urls'))
    # path('json/example', update_model_detail_view),
    # path('json/cbv', JsonView.as_view()),
    # path('json/cbv2', JsonView2.as_view()),
    # path('json/serializer', SerializedDetailView.as_view()),
    # path('json/serializers', SerializerListView.as_view())
]
